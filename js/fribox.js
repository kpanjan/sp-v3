$(document).ready(function() {
	function startLoading() {
		$(".loading").show();
	}
	
	function stopLoading() {
		$(".loading").hide();
	}
	
	function addFile(file) {
		var size = file.size;
		var unit = "B";
		
		if (size > 1024) {
			size = Math.round(size/1024);
			unit = "KiB";
		}
		if (size > 1024) {
			size = Math.round(size/1024);
			unit = "MiB";
		}
		if (size > 1024) {
			size = Math.round(size/1024);
			unit = "GiB";
		}
		
	/* tukaj nekaj manjka */	
		    
	}
	
	function removeFile() {
		startLoading();
		setTimeout(stopLoading, 1000);
	}
	
	function uploadFile(event) {
		var filename = $("#datoteka").val().replace(/.*\\/g, "");
		var size = Math.round(Math.random(100)*10000000);
		
		if (filename != "") {
			addFile({name: filename, size: size});
		}
		
		
		event.preventDefault();
		$("#datoteka").val("");
	}
	
	// INIT PAGE
	$(".loading").hide();
	$("#nalozi").click(uploadFile);
	
	var files = [
		{name: "jony_depp.png", size: 10024552},
		{name: "SP Izpit 2018.docx", size: 998654},
		{name: "Private Deer Movie.mp4", size: 987632730},
	];
	
	for (var i=0; i<files.length; i++) {
		addFile(files[i])
	}

});
